package main

import (
	context "context"
	"fmt"

	stationpb "gitlab.com/PatrickVienne/go-grpc-mongo/proto/station"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	"gopkg.in/mgo.v2/bson"
)

func (s *stationpb.StationServiceServer) CreateStation(ctx context.Context, req *stationpb.CreateStationReq) (*stationpb.CreateStationRes, error) {
	// Essentially doing req.Station to access the struct with a nil check
	reqStation := req.GetStation()
	// Now we have to convert this into a StationModel type to convert into BSON
	data := stationpb.StationModel{
		// ID:    Empty, so it gets omitted and MongoDB generates a unique Object ID upon insertion.
		AuthorID: reqStation.GetAuthorId(),
		Title:    reqStation.GetTitle(),
		Content:  reqStation.GetContent(),
	}

	// Insert the data into the database, result contains the newly generated Object ID for the new document
	result, err := stationdb.InsertOne(mongoCtx, data)
	// check for potential errors
	if err != nil {
		// return internal gRPC error to be handled later
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Internal error: %v", err),
		)
	}
	// add the id to station, first cast the "generic type" (go doesn't have real generics yet) to an Object ID.
	oid := result.InsertedID.(primitive.ObjectID)
	// Convert the object id to it's string counterpart
	reqStation.Id = oid.Hex()
	// return the station in a CreateStationRes type
	return &stationpb.CreateStationRes{Station: reqStation}, nil
}

func (s *stationpb.StationServiceServer) ReadStation(ctx context.Context, req *stationpb.ReadStationReq) (*stationpb.ReadStationRes, error) {
	// convert string id (from proto) to mongoDB ObjectId
	oid, err := primitive.ObjectIDFromHex(req.GetId())
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Could not convert to ObjectId: %v", err))
	}
	result := stationdb.FindOne(ctx, bson.M{"_id": oid})
	// Create an empty StationModel to write our decode result to
	data := stationpb.StationModel{}
	// decode and write to data
	if err := result.Decode(&data); err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Could not find station with Object Id %s: %v", req.GetId(), err))
	}
	// Cast to ReadStationRes type
	response := &stationpb.ReadStationRes{
		Station: &stationpb.Station{
			Id:       oid.Hex(),
			AuthorId: data.AuthorID,
			Title:    data.Title,
			Content:  data.Content,
		},
	}
	return response, nil
}

func (s *stationpb.StationServiceServer) DeleteStation(ctx context.Context, req *stationpb.DeleteStationReq) (*stationpb.DeleteStationRes, error) {
	// Get the ID (string) from the request message and convert it to an Object ID
	oid, err := primitive.ObjectIDFromHex(req.GetId())
	// Check for errors
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Could not convert to ObjectId: %v", err))
	}
	// DeleteOne returns DeleteResult which is a struct containing the amount of deleted docs (in this case only 1 always)
	// So we return a boolean instead
	_, err = stationdb.DeleteOne(ctx, bson.M{"_id": oid})
	// Check for errors
	if err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Could not find/delete station with id %s: %v", req.GetId(), err))
	}
	// Return response with success: true if no error is thrown (and thus document is removed)
	return &stationpb.DeleteStationRes{
		Success: true,
	}, nil
}

func (s *stationpb.StationServiceServer) UpdateStation(ctx context.Context, req *stationpb.UpdateStationReq) (*stationpb.UpdateStationRes, error) {
	// Get the station data from the request
	station := req.GetStation()

	// Convert the Id string to a MongoDB ObjectId
	oid, err := primitive.ObjectIDFromHex(station.GetId())
	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Could not convert the supplied station id to a MongoDB ObjectId: %v", err),
		)
	}

	// Convert the data to be updated into an unordered Bson document
	update := bson.M{
		"authord_id": station.GetAuthorId(),
		"title":      station.GetTitle(),
		"content":    station.GetContent(),
	}

	// Convert the oid into an unordered bson document to search by id
	filter := bson.M{"_id": oid}

	// Result is the BSON encoded result
	// To return the updated document instead of original we have to add options.
	result := stationdb.FindOneAndUpdate(ctx, filter, bson.M{"$set": update}, options.FindOneAndUpdate().SetReturnDocument(1))

	// Decode result and write it to 'decoded'
	decoded := stationpb.StationModel{}
	err = result.Decode(&decoded)
	if err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Could not find station with supplied ID: %v", err),
		)
	}
	return &stationpb.UpdateStationRes{
		Station: &stationpb.Station{
			Id:       decoded.ID.Hex(),
			AuthorId: decoded.AuthorID,
			Title:    decoded.Title,
			Content:  decoded.Content,
		},
	}, nil
}

func (s *stationpb.StationServiceServer) ListStations(req *stationpb.ListStationsReq, stream stationpb.StationService_GetStationsServer) error {
	// Initiate a StationModel type to write decoded data to
	data := &stationpb.StationModel{}
	// collection.Find returns a cursor for our (empty) query
	cursor, err := stationdb.Find(context.Background(), bson.M{})
	if err != nil {
		return status.Errorf(codes.Internal, fmt.Sprintf("Unknown internal error: %v", err))
	}
	// An expression with defer will be called at the end of the function
	defer cursor.Close(context.Background())
	// cursor.Next() returns a boolean, if false there are no more items and loop will break
	for cursor.Next(context.Background()) {
		// Decode the data at the current pointer and write it to data
		err := cursor.Decode(data)
		// check error
		if err != nil {
			return status.Errorf(codes.Unavailable, fmt.Sprintf("Could not decode data: %v", err))
		}
		// If no error is found send station over stream
		stream.Send(&stationpb.ListStationsRes{
			Station: &stationpb.Station{
				Id:       data.ID.Hex(),
				AuthorId: data.AuthorID,
				Content:  data.Content,
				Title:    data.Title,
			},
		})
	}
	// Check if the cursor has any errors
	if err := cursor.Err(); err != nil {
		return status.Errorf(codes.Internal, fmt.Sprintf("Unkown cursor error: %v", err))
	}
	return nil
}
